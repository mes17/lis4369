> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>


# LIS4369 - Extensible Solutions

## Matthew Stoklosa

### Assignment 1 Requirements:
Four Parts:

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket Repo links:
    
    a) this assignment and 
    
    b) the completed toutorial bitbucketlocations

#### README.md file should include the following items:

* Screenshot of a1_tip_calculator 
* git commands w/short description 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Creates a new local repository with the specified name
2. git status - Lists all new or modified files to be commited
3. git add - Snapshots the file in preparation for versioning
4. git commit - Records file snapshots permanently in version history  
5. git push - Uploads all local branch commits to GitHub
6. git pull - Downloads bookmark history and incorporates changes
7. git branch - Lists all local branches in the current repository

#### Assignment Screenshots:

*Screenshot of Tip calculator running on Idle*:

![Tip calculator idle Screenshot](img/a1_idle.png)

*Screenshot of Tip calculator running in Visual*:

![Tip calculator visual Screenshot](img/a1_visual.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
