#!/usr/bin/envpython3

# Program requirments:
print("Tip calculator")
print("\nProgram requirements:\n"
        +"1. Must use float data type for user input (except, \"Party Number\").\n"
        + "2. Must round calculations to two decimal places.\n"
        + "3. Must format currency with dollar sign, and two decimnal places.")

print("\n User Input Input:")
meal_cost = float(input("cost of meal: "))
tax_percent = float(input("tax percent: "))
tip_percent = float(input("tip percent: "))
people_num = int(input("Party number: "))

# calculate tax, tip and total amount
tax_amount = round(meal_cost * (tax_percent / 100),2) #convert to percentage
due_amount = round(meal_cost + tax_amount,2)
tip_amount = round((due_amount)*(tip_percent/100),2) # percentage of cost + tax
total = round(meal_cost+tax_amount+tip_amount,2)
split = round(total / people_num,2)

# display results
# Formatting:
# https://docs.python.org/3/libaray/string.html#format-specification-mini-language
# https://docs.python.org/3/libaray/string.html#formatexamples
# https://mkaz.blog/code/python-string-format-cook/
# old style: https://docs.python.org/3/libarary/studypes.html#string-formatting
print("\nProgram Output:")
print("Subtotal:\t","${0:,.2f}".format(meal_cost)) # subtotal
print("Tax: \t\t","${0:,.2f}".format(tax_amount))
print("Amount Due:\t","${0:,.2f}".format(due_amount))
print("Gratuity:\t","${0:,.2f}".format(tip_amount))
print("Total:\t\t","${0:,.2f}".format(total))
print("Split " + "(" + str(people_num)+"):\t","${0:,.2f}".format(split))