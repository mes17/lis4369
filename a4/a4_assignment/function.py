#!/usr/bin/env python3
import re
import numpy as np
import pandas as pd
np.set_printoptions(threshold=np.inf)
import matplotlib.pyplot as plt
from matplotlib import style

df = 0.0

#url = "https://raw.githubusercontent.com/vincentarelbundock/Rdatasets/master/csv/COUNT/titanic.csv"
url = "https://raw.githubusercontent.com/vincentarelbundock/Rdatasets/master/csv/Stat2Data/Titanic.csv"


def get_requirments():
    print("Assignment 4")
    print("\nProgram requirements:\n"
        + "1. Run demo.py.\n"
        + "2. If erros, more than likely missing installations.\n"
        + "3. Test Python Package Installer: pip freeze\n"
        + "4. Reserach how to install any missing packages:\n"
        + "5. Create at least three functions that are called by the program: \n"
        + "     a. main(): calls at least two other fuctions \n"
        + "     b. get_requirments(): displays the program requirements.\n"
        + "     c. data_analysis_2(): displaysthe following data.\n"
        + "6. Display graph as per instructions w/in demo.py")

def data_analysis_2():
    #url = "https://raw.githubusercontent.com/vincentarelbundock/Rdatasets/master/csv/Stat2Data/Titanic.csv"
    #url = "https://vincentarelbundock.github.io/Rdatasets/"
    #url = "https://raw.githubusercontent.com/vincentarelbundock/Rdatasets/master/csv/COUNT/titanic.csv"
    #url = "https://raw.github.com/vincentarelbundock/Rdatasets/master/csv/COUNT/titanic.csv"

    #except OSError as e: print('not working')
    print("***DataFrame composed of three components: index, columns, and data. Data also known as values.***")
    df = pd.read_csv(url)
    
    index = df.index
    columns = df.columns
    values = df.values

    print("\n1. Print indexes:")
    print(index)

    print("\n2. Print columns:")
    print(columns)

    print("\n3. Print columns (another way):")
    print(df.columns[:])

    print("\n4. Print (all) values, in array format:")
    print(values)

    print("\n5. ***Print components data types:***")
    print("\na) index type:")
    print(type(index))
    #pandas.core.inedexes.base.Index

    print("\nb) values type:")
    print(type(columns))
    #pandas.core.inedexes.base.Index

    print("\nc) values types:")
    print(type(values))
    #numpy.ndarray

    print("\n6. Print summary of DataFrame (similar to describe tablename; in MYSQL):")
    print(df.info())

    print("\n7. First five lines (all columns):")
    print(df.head())

    df = df.drop('Unnamed: 0',1)
    print("\n8. Print summary of DataFrame (after dropping column 'Unnamed: 0'):")
    print(df.info())

    print("\n9. First five lines (after dropping column 'Unnamed: 0'):")
    print(df.head())

    print("\n***Precise data selection (data slicing):***")
    print("\n10. Using iloc, return first 3 rows:")
    print(df.iloc[:3])
    #print(df.iloc[0:3:1])

    print("\n11. Using iloc, return last 3 rows (Start on index 1310 to end):")
    print(df.iloc[1310:])

    print("\n12. Select rows 1, 3, and 5; andcolumns 2,4, and 6 (incluneds index column):")
    a = df.iloc[[0,2,4],[1,3,5]]
    print(a)

    print("\n13. Select all rows; and columns 2,4, and 6 (includes index column):")
    a = df.iloc[:,[1,3,5]]
    print(a)

    print("\n14. Select all rows 1, 3, and 5; and all columns (includes index column):")
    a = df.iloc[[0,2,4],:]
    print(a)
    #a=df.iloc[[0,2,4]]
    #print(a)
    
    print("\n15. Select all rows, and all columns (includes index column) Note: only first and last 30 records displayed:")
    a = df.iloc[:,:]
    print(a)

    print("\n16. Select all rows, and all columns, starting at column 2 (incudes index column). Note: only first and last 30 records displayed:")
    a = df.iloc[:,1:]
    print(a)

    print("\n17.Select row 1, and column 1, (includes index column):")
    a = df.iloc[0:1, 0:1]
    print(a)

    print("\n18.Select rows 3-5 and columns 3-5, (incudes index column):")
    # Note: .iloc does *not* contain last index value--here, should have included 5!
    a = df.iloc[2:5 , 2:5]
    print(a)

    print("\n19. ***Convert pandas DataFrame df to NumPy, use values command:***")
    #select all rows, amnd all columns, starting at column 2:
    b = df.iloc[:,1:].values #ndarray = N-dimensional array (rows and columns)

    print("\n20. Print data fname type:")
    print(type(df))

    print("\n21. Print a type:")
    print(type(a))

    print("\n22. Print b type")
    print(b.dtype)
    
    print("\n23. Print number of dimesnsions and items in array (rowm columns). Remember: starting at colunm 2:")
    print(b.shape)

    print("\n24. Print type of iteams in array. Remember: ndarray is aarray of arrays. Each record/item is an array.")
    print(b.type)

    print("\n25. Printing a:")
    print(a)

    print("\n26. Length a:")
    print(len(a))

    print("\n27. Printing b:")
    print(b)

    print("\n28. Length b: ")
    print(len(b))

    # Print element of ndarray b in *second* rowm *third* column
    print("\n29. Print element of (NumPy array) ndarray b in *second* row, *third* column:")
    print(b[1,2])

    #Print full NumPy array, no ellipsis: here is why np.set_printoptions(threshold=np.inf) is set at top of file
    print("\n30. Print all records for NumPy array column 2: ")
    print(b[:,1])

    print("\n31. Get passenger names:")
    names = df["Names"]
    print(names)

    print("\n32. Find all passerngers with name 'Allison' (using regualr expressions):")
    for name in names:
        print(re.search(r'(Allison)',name))
    #with pd.option_contect('display.max_rows', None):
    # print(df) # print entire datafname

    print("\n***33. Statistcal Analysis (DataFrame notation):***")
    print("\na) Print mean age:")
    avg = df["Age"].mean()
    print(avg)

    print("\nb) Print mean age, rounded to two decimal places:")
    avg = df["Age"].mean().round(2)
    print(avg)

    print("\nc) Print mean of every column in DataFrame (may not be suitable with creatin columns):")
    avg_all = df.mean(axis=0)
    #avg_all - df.mean(axis=1)
    print(avg_all)

    print("\nd) Print summary staticis (DataFrame notation):")
    describe = df["Age"].describe()
    #describe = df["Age"].describe(percentiles=[.10, .20, .50, .80])
    print(describe)

    print("\ne) Print minimum age (DateFrame notation):")
    min - df["Age"].min()
    print(min)

    print("\nf) Print maximum age (DateFrame notation):")
    max = df["Age"].max()
    print(max)

    print("\ng) Print median age (DateFrame notation):")
    median = df["Age"].median()
    print(median)

    print("\nh) Print mode age (DateFrame notation):")
    mode = df["Age"].mode()
    print(mode)

    print("\ni) Print number of values (DateFrame notation):")
    count = df["Age"].count()
    print(count)

    print("\n*** Graph: Dispay ages of the first 20 passagners (use code for previous assignment):***")
    
    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()

