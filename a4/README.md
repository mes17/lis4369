> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Solutions

## Matthew Stoklosa

### Assignment 4 Requirements:


Development:

1.Requirements:

a. Code and run demo.py.
(Note: *be sure* necessary packages are installed!)

Note: If needed, see previous assignment for installing Python packages.

b. Then use demo.py to backward-engineer the screenshots below it.

c. When displaying the required graph (see code below), answer the following question:Why is the graph line split?

2.Be sure to test your program using both IDLEand Visual Studio Code.
 

#### README.md file should include the following items:

* Screenshot of Assignment 4

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Assignment 4 running on Idle*:

![Assignment 4 Idle Screenshot](img/a4_idle_1.png)

*Screenshot of Assignment 4 running on Idle*:

![Assignment 4 Idle Screenshot](img/a4_idle_2.png)
*Screenshot of Assignment 4 running on Idle*:

![Assignment 4 Idle Screenshot](img/a4_idle_3.png)

*Screenshot of Assignment 4 running in Visual*:

![Assignment 4 Visual Screenshot](img/a4_visual_1.png)

*Screenshot of Assignment 4 running in Visual*:

![Assignment 4 Visual Screenshot](img/a4_visual_2.png)

*Screenshot of Assignment 4 running in Visual*:

![Assignment 4 Visual Screenshot](img/a4_visual_3.png)

*Screenshot of Assignment 4 graph*:

![Assignment 4 graph Screenshot](img/a4_graph.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
