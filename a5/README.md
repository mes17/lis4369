> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Solutions

## Matthew Stoklosa

### Assignment 5 Requirements:


Development:

1. Requirements:

    a. Complete the following tutorial: Introduction_to_R_Setup_and_Tutorial
   
    b. Code and run lis4369_a5.R(see below)
   
    c. Be sure to include at least two plotsin your README.md file.
    
2. Be sure to test your program using RStudio.

#### README.md file should include the following items:

* Screenshot of Assignment 5

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Assignment 5 running in Rstudio*:

![Assignment 5 in Rstudio Screenshot](img/a5_1.png)

*Screenshot of Assignment 5 running in Rstudio*:

![Assignment 5 in Rstudio Screenshot](img/a5_2.png)

*Screenshot of Assignment 5 running in Rstudio*:

![Assignment 5 in Rstudio Screenshot](img/a5_3.png)

*Screenshot of Assignment 5 running in Rstudio*:

![Assignment 5 in Rstudio Screenshot](img/a5_4.png)

*Screenshot of Assignment 5 running in Rstudio*:

![Assignment 5 in Rstudio Screenshot](img/a5_9.png)

*Screenshot of Assignment 5 running in Rstudio*:

![Assignment 5 in Rstudio Screenshot](img/a5_10.png)

*Screenshot of Assignment 5 running in Rstudio*:

![Assignment 5 in Rstudio Screenshot](img/a5_11.png)

*Screenshot of Assignment 5 running in Rstudio*:

![Assignment 5 in Rstudio Screenshot](img/a5_12.png)

*Screenshot of Assignment 5 running in Rstudio*:

![Assignment 5 in Rstudio Screenshot](img/a5_5.png)

*Screenshot of Assignment 5 running in Rstudio*:

![Assignment 5 in Rstudio Screenshot](img/a5_6.png)

*Screenshot of Assignment 5 running in Rstudio*:

![Assignment 5 in Rstudio Screenshot](img/a5_7.png)

*Screenshot of Assignment 5 running in Rstudio*:

![Assignment 5 in Rstudio Screenshot](img/a5_8.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
