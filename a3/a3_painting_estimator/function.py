#!/usr/bin/env python3
def get_requirments():
    print("Painting Estimator")
    print("\nProgram requirements:\n"
        + "1. Calculate home interior pain cost (w/o primer).\n"
        + "2. Must use float data type.\n"
        + "3. Must use SQFT_PER_GALLON constant. \n"
        + "4. Must use interation structure (aka loop).\n"
        + "5. Format, right-align numbers, and round to two decimal places. \n"
        + "6. Create at least five functions that are called by the program:\n"
        + "     a. main(): calls two other functions: get_requirments() and estimate_painting_cost()\n"
        + "     b. get_requirments(): displays the program requirments.\n"
        + "     c. estimate_paint_cost(): calculates interior home painting, and calls print functions. \n"
        + "     d. print_painting_estimate(): display painting costs.\n"
        + "     e. print_painitng_percentage(): display painting costs percentages.")

def estimate_painting_cost():

    #initalize varibales
    total_interior_sqft = 0.0
    price_per_gallon_paint = 0.0
    hourly_painting_rate_per_sqft = 0.0

    #get user data
    print("\nInput:")
    total_interior_sqft = (float(input("Enter total interior sq ft: ")))
    price_per_gallon_paint = (float(input("Enter price per gallon paint: ")))
    hourly_painting_rate_per_sqft = (float(input("Enter hourly painting rate per sq ft: ")))
    #calulations
    total_sqft = total_interior_sqft
    sqft_per_gallon = 350.00
    number_of_gallons = (total_interior_sqft / sqft_per_gallon)
    paint_per_gallon = price_per_gallon_paint
    labor_per_sqft = hourly_painting_rate_per_sqft

    paint_cost = (number_of_gallons * paint_per_gallon)
    labor_cost = (total_sqft * labor_per_sqft)
    total_cost = (paint_cost + labor_cost)

    paint_percent = (paint_cost / total_cost)
    labor_percent = (labor_cost / total_cost)
    total_percent = (paint_percent + labor_percent)

    # def print_painting_estimate(total_sqft, sqft_per_gallon, number_of_gallons,paint_per_gallon, labor_per_sqft):
    # display results
    print("\nOutput:")
    print("{0:17} {1:>15}".format("Item", "Amount"))
    print("{0:17} \t {1:>8,.2f}".format("Total sq ft:", total_sqft))
    print("{0:17} \t {1:>8,.2f}".format("Sq Ft per Gallon:", sqft_per_gallon))
    print("{0:17} \t {1:>8,.2f}".format("Number of Gallons:", number_of_gallons))
    print("{0:17} \t ${1:>7,.2f}".format("Paint per Gallon:", paint_per_gallon))
    print("{0:17} \t ${1:>7,.2f}".format("Labor per Sq Ft:", labor_per_sqft))
    
    # def print_painitng_percentage(paint_cost, paint_percent,labor_cost,labor_percent, total_cost, total_percent):
    #cost
    print("{0:17} {1:>9} {2:>17}".format("\nCost", "Amount", "Percentage"))
    print("{0:17} ${1:>8,.2f} {2:>17.2%}".format("Paint:", paint_cost, paint_percent))
    print("{0:17} ${1:>7,.2f} {2:>17.2%}".format("Labor:", labor_cost, labor_percent))
    print("{0:17} ${1:>7,.2f} {2:>17.2%}".format("Total:", total_cost, total_percent))
    
    # ask for the loop
    again = (input("\nEstimat another paint job? (y/n): ")) 
    
    # loop 
    if (again == "y"):
        estimate_painting_cost()

    elif (again == "n"):
        print("Thank you for using our Painting Estimator!")
        print("Please see our website: https://bitbucket.org/mes17/lis4369/src/master/")