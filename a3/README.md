> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Solutions

## Matthew Stoklosa

### Assignment 3 Requirements:


1. Backward-engineer (using Python) the following screenshots:

2. The program should be organized with two modules (See Ch. 4)
   
    a.functions.pymodulecontains the following functions:
    
    i. get_requirements()
        
    ii. estimate_painting_cost()
        
    iii. print_painting_estimate()
    
    iv. print_painting_percentage()

    b.main.pymodule imports the functions.pymodule, and callsthe functions.
    
3. Be sure to test your program using both IDLEand Visual Studio Code.

4. Be Sure to carefully review (How to Write Python): https://realpython.com/lessons/what-pep-8-and-why-you-need-it/
 

#### README.md file should include the following items:

* Screenshot of a3_painting_estimator

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Painting Estimator running on Idle*:

![Painting Estimator Idle Screenshot](img/a3_idle.png)

*Screenshot of Painting Estimator running in Visual*:

![Painting Estimator Visual Screenshot](img/a3_visual.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
