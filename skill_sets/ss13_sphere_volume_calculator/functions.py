#!/usr/bin/env python3

import math

def get_requirements():
    
    print("Sphere Volume Calculator")
    print(" ")
    print("\nProgram Requirments:\n"
        "1. Program calculates sphere volume in liquid U.S. gallons from user-entered diameter vlaue in inches, and rounds to two decimal places.\n"
        "2. Must use Phyton's *built-in* PI and pow() capabilities. \n"
        "3. Program checks for non-intergers and non-numeric values.\n"
        "4. Program continues to prompt for user entry until no longer requested, prompt acepts upper of lower case letters.\n")
        
def sphere_volume_calculator(): 

    diameter = 0
    volume = 0.0
    gallons = 0.0
    choice = ''

    print("Input: ")
    choice = input("do you want to calculate a sphere volume (y or n)?").lower()
    print("\nOutput: ")

    while (choice[0] == 'y'):
        diameter = input("Please enter diameter in inches: ")
        test = True
        while (test == True):
            try:
                diameter = int(diameter)
                volume = ((4.0/3.0) * math.pi * math.pow(diameter/2.0,3))
                gallons = volume/231
                print("\nSphere volume: {0:,.2f} liquid US gallons\n".format(gallons))
                test = False

            except ValueError:
                print("\nNot valid integer!")
                diameter = input("please enter diameter in inches: ")
            continue
        choice = input("do you want to calculate another sphere volume?").lower()

    print("\nThank you for using our Sphere Volume Calculator!")