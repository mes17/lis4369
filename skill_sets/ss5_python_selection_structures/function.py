#!/usr/bin/env python3
def get_requirements():
# Program requirments:
    print("Python Selection Structures")
    print("\nProgram requirements:\n"
        + "1. Use Python selection structure.\n"
        + "2. Prompt user for two numbers, and a suitable operator.\n"
        + "3. Test for correct numeric operator.\n"
        + "4. Replicate display below.")

def python_cal():
    #initalize varibales
    num1 = 0.0
    num2 = 0.0
    op = 0.0

    #get user data
    print("\nInput:")
    num1 = float(input("Enter num1: "))
    num2 = float(input("Enter num2: "))
    print("Suitable Operators: +,-,*,/,//(interger division), %(modulo operator),**(power)")
    op = str(input("Enter operator: "))

    #random number 

    if (op =="+"):
        result = num1 + num2
        print(result)
    elif (op =="-"):
        result = num1 - num2
        print(result)
    elif (op =="*"):
        result = num1 * num2
        print(result)
    elif (op =="/"):
        result = num1 / num2
        print(result)
    elif (op =="//"):
        result = num1 // num2
        print(result)
    elif (op =="%"):
        result = num1 % num2
        print(result)
    elif (op =="**"):
        result = num1 ** num2
        print(result)
        #result = pow(num1, num2)
    else:
        print("Incorrect operator!")