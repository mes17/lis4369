#!/usr/bin/envpython3

# Program requirments:
print("Miles Per Gallon")
print("\nProgram requirements:\n"
        + "1. Covert MPG.\n"
        + "2. Must use float data type for user input and calculation.\n"
        + "3. Format and round conversion to two decimnal places.")

#Get the miles and gallon
print("\nInput:")
miles = float(input("Enter miles driven: "))
gallon = float(input("Enter gallon of fuel used: "))

#calculate the MPG
mpg = miles/gallon
mpg = round(mpg,3)

# display results
print("\nOutput:")
print(f'{miles:.2f} miles driven and {gallon:.2f} gallons used = {mpg:.2f} mpg')
