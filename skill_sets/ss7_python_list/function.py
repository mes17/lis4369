#!/usr/bin/env python3
def get_requirements():
# Program requirments:
    print("Python List")
    print("\nProgram requirements:\n"
        + "1. Lists (python data structure): mutable, ordered sequence of elements.\n"
        + "2. Lists are mutable/changeable--that is, can insert, update, delete.\n"
        + "3. Create list - using square brackets [list]: my_list = [cherries, apple, bananas, oranges ] \n"
        + "4. Create a program that mirros the following IPO (input/process/output) format.\n."
        + "Note: user enters number of requested list elemetns, dynmically rendered bleow (that its, number of elements can change each run). ")

def python_list():

#initalize varibales


#get user data
    print("\nInput:")
    lists = (input("Enter number of list elements:"))
    print("")
    e1 = (input("Please enter list elements 1: "))
    e2 = (input("Please enter list elements 2: "))
    e3 = (input("Please enter list elements 3: "))
    e4 = (input("Please enter list elements 4: "))

#calculate

    # display results
    lists = [e1,e2,e3,e4]
    print("\nOutput:")
    print("\nPrint my lists:")
    print(lists)
    e5 = (input("Please enter lists elements: "))
    pos = int(input("Please enter lists *index* position (note: must convert to int): "))
    lists.insert(pos,e5)
    print(lists)
    print("\nCount number of elements in lists")
    print(len(lists))
    print("\nSort elements in list alphabetically: ")
    lists.sort()
    print(lists)
    print("\nReverse lists: ")
    lists.reverse()
    print(lists)
    print("\nRemove last list elembents: ")
    del lists[-1]
    print(lists)
    print('\nDelete second element from list by *index* (note: 1=2nd element):  ')
    lists.pop(1)
    #del lists[1]
    print(lists)
    print("\nDelete element from list by *value* (cherries): ")
    lists.remove("cherries")
    print(lists)
    print("\nDelete all elements from lists: ")
    lists.clear()
    print(lists)
