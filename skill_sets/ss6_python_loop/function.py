#!/usr/bin/env python3
def get_requirements():
# Program requirments:
    print("Python Looping Structures ")
    print("\nProgram requirements:\n"
        + "1. Print while loop.\n"
        + "2. Print for loops using range() function, and implicit and explicit list.\n"
        + "3. Use break and continue statements.\n"
        + "4. Replicate display below.\n"
        + "Note: In Python, for loop used for iterating over a sequence (i.e., list, tuple, dictionary, set, or string).")

def python_looping_structures():
    #initalize varibales
    print("\n1. while loop:")
    i = 1
    while i <=3:
        print(i)
        i += 1
    
    print("\n2. for loop: using range() function with 1 arg")
    for i in range(4):
        print(i)

    print("\n3. for loop: using range() function with two args")
    for i in range(1,4):
        print(i)

    print("\n4. for loop: using () function with three args (interval 2):")
    for i in range (1,4,2):
        print(i)

    print("\n5. for loopL using range() function with three args (njegative interaval): ")
    for i in range(3,-1,-2):
        print(i)

    print("\n6. for loop using (implicit) list:")
    for i in [1,2,3]:
        print(i)

    print("\n7. for loop interating through (explicit) string list:")
    list1 = ["Michigan","Alabama","Florida"]
    for i in list1:
        print(i)

    print("\n8. for loop using break statement (stops loop):")
    for i in list1:
        if i == "Florida":
            break
        print(i)
        
    print("\n9. for loop using contiune statement (stops and continues with next):")
    for i in list1:
        if i == "Alabama":
            continue
        print(i)

    print("\n10. print list length:")
    print(len(list1))