#!/usr/bin/env python3
def get_requirements():
# Program requirments:
    print("Miles Per Gallon")
    print("\nProgram requirements:\n"
        + "1. Covert MPG.\n"
        + "2. Must use float data type for user input and calculation.\n"
        + "3. Format and round conversion to two decimnal places.")

def calculate_miles_per_gallon():
    #initalize varibales
    miles = 0.0
    gallons = 0.0
    mpg = 0.0

#get user data
    print("\nInput:")
    miles = float(input("Enter miles driven: "))
    gallons = float(input("Enter gallon of fuel used: "))

#calculate the MPG
    mpg = miles / gallons

# display results
    print("\nOutput:")
    print("{0:,.2f}{1}{2:,.2f}{3}{4:,.2f}{5}".format(
    miles," miles driven and ",gallons," gallons used = ",mpg," mpg"))