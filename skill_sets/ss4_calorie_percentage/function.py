#!/usr/bin/env python3
def get_requirements():
# Program requirments:
    print("Calorie Percentage")
    print("\nProgram requirements:\n"
        + "1. Find calories per grams of fat, carbs, and protein.\n"
        + "2. Calulate percentages.\n"
        + "3. Must use float data types.\n"
        + "4. Format, right-align number, and round to two decimal places.")

def calorie_percentage():
    #initalize varibales
    fat = 0.0
    carb = 0.0
    protein = 0.0
    total = 0.0
    fat_c = 0.0
    carb_c = 0.0
    protein_c = 0.0
    fat_p = 0.0
    carb_p = 0.0
    protein_p = 0.0


    #get user data
    print("\nInput:")
    fat = float(input("Enter total fat grams: "))
    carb = float(input("Enter total carb grams: "))
    protein = float(input("Enter total protein grams: "))
    
    #calculate the calories and percentage

    fat_c = (fat * 9)
    carb_c = (carb * 4)
    protein_c = (protein * 4)
    total = (fat_c + carb_c + protein_c)
    fat_p = (fat_c / total)
    carb_p = (carb_c / total)
    protein_p = (protein_c / total)

    # display results
    print("\nOutput:")
    print("{0:17} {1:17} {2:17}".format("Type", "Calories", "Percentage"))
    print("{0:17} {1:,.2f} {2:>19.2%}".format("Fat:", fat_c, fat_p))
    print("{0:17} {1:,.2f} {2:>19.2%}".format("Carbs:", carb_c, carb_p))
    print("{0:17} {1:,.2f} {2:>19.2%}".format("Protein:", protein_c, protein_p))