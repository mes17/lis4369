#!/usr/bin/env python3
def get_requirements():
# Program requirments:
    print("IT/ICT Student Percentage")
    print("\nProgram requirements:\n"
        + "1. Find number of IT/ICT students in class\n"
        + "2. Calulate IT/ICT students in class.\n"
        + "3. Must use float data type (to facilitate right-alignment) \n"
        + "4. Format, right-align number, and round to two decimal places.")

def calculate_it_ict_student_perentage():
    #initalize varibales
    it = 0.0
    ict = 0.0
    total = 0.0
    it_p = 0.0
    ict_p = 0.0

#get user data
    print("\nInput:")
    it = float(input("Enter number of IT students: "))
    ict = float(input("Enter number of ICT students: "))

#calculate the IT / ICT
    total = (it + ict)
    it_p = (it / total)
    ict_p = (ict / total)

    # display results
    print("\nOutput:")
    print("{0:17} {1:>5.2f}".format("Total Students:", total))
    print("{0:17} {1:>5.2%}".format("IT Students:", it_p))
    print("{0:17} {1:>5.2%}".format("ICT Students:", ict_p))