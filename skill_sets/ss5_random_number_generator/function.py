#!/usr/bin/env python3
def get_requirements():
# Program requirments:
    print("Calorie Percentage")
    print("\nProgram requirements:\n"
        + "1. Get user beginning and ending integer values, and store in two variables\n"
        + "2. Display 10 random numbers between, and including, above values.\n"
        + "3. Must use interger data types.\n"
        + "4. Example 1: Using range() and randiant() functions.\n"
        + "5. Example 2: Using a list with range() and shuffle() functions.")

def random_number():
    #initalize varibales
    start = 0.0
    end = 0.0
    example_1 = 0.0
    example_2 = 0.0

    #get user data
    print("\nInput:")
    start = int(input("Enter beginning value: "))
    end = int(input("Enter ending value: "))
    
    #random number 
    example_1 = start
    example_2 = end

    # display results
    print("\nOutput:")
    print("{0:17} {1:0}".format("Example 1: Using range() and randint() functions: \n", example_1))
    print("{0:17} {1:0}".format("Example 2: Using a list, with range() and shuffle() functions: \n", example_2))