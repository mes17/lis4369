import functions as f

def main():
    f.get_requirements()
    f.temperature_conversion_program()
    
if __name__ == "__main__":
    main()     