> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Solutions

## Matthew Stoklosa

### Assignment 2 Requirements:


1. Backward-engineer (using Python) the following screenshots:
2. The program should be organized with two modules 
    
    a) functions.py module contains the following functions: 
    
    i) get_requirements()

    ii) calculate_payroll()
           
    iii) print_pay


    b) main.py module imports the functions.py module, and calls the functions.
3. Be sure to test your program using both IDLE and Visual Studio Code.
 

#### README.md file should include the following items:

* Screenshot of a2_payroll

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Payroll No Overtime running on Idle*:

![Payroll No Overtime Idle Screenshot](img/a2_no_idle.png)

*Screenshot of Payroll No Overtime running in Visual*:

![Payroll No Overtime Visual Screenshot](img/a2_no_visual.png)

*Screenshot of Payroll with Overtime running on Idle*:

![Payroll with Overtime idle Screenshot](img/a2_w_idle.png)

*Screenshot of Payroll with Overtime running in Visual*:

![Payroll with Overtime Visual Screenshot](img/a2_w_visual.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
