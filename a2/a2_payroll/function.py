#!/usr/bin/env python3
def get_requirments():
    print("Payroll Calculator")
    print("\nProgram requirements:\n"
        + "1. Must use float data type for user input.\n"
        + "2. Overtime rate: 1.5 times hourly rate (hours over 40).\n"
        + "3. Holiday rate: 2.0 times hourly rate (all holiday hours). \n"
        + "4. Must format currency with dollar sign, and round to two decimal places.\n"
        + "5. Create at least thre functions that are called by the program: \n"
        + "     a. main(): calls at least two other functions.\n"
        + "     b. get_requirments(): displays the program requirements. \n"
        + "     c. calculate_payroll(): calculates an individual one-week paycheek.")

def calculate_payroll():

#initalize varibales
    hours = 0.0
    holiday_hour = 0.0
    payrate = 0.0
    regularpay = 0.0
    overtimehours = 0.0
    holiday = 0.0
    grosspay = 0.0
    overtimepay = 0.0
    overtimerate = 0.0
    hours = round(40,2)

#get user data
    print("\nInput:")
    hours = (float(input("Enter hours worked: ")))
    holiday_hour = (float(input("Enter holiday hours: ")))
    payrate = (float(input("Enter hourly pay rate: ")))

 #calculate <40 hours

    if hours < 41:
        regularpay = round(hours * payrate, 2)
        holiday = round(holiday_hour * 20, 2)
        grosspay = round(regularpay+holiday,2)


 #calculate >40 hours

    elif hours > 40:
        overtimehours = round(hours - 40.00,2)
        regularpay = round(40 * payrate, 2)
        overtimerate = round(payrate * 1.5, 2)
        overtimepay = round(overtimehours * overtimerate)
        holiday = round(holiday_hour * 20, 2)
        grosspay = round(regularpay+overtimepay+holiday,2)

# display results
    print("\nOutput:")
    print("Base:\t\t","${0:,.2f}".format(regularpay))
    print("Overtime:\t","${0:,.2f}".format(overtimepay))
    print("Holiday:\t","${0:,.2f}".format(holiday))
    print("Gross:\t\t","${0:,.2f}".format(grosspay))