> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Solutions

## Matthew Stoklosa

### LIS4369  Requirements:

 Course Work Links:

1. [A1 README.md](https://bitbucket.org/mes17/lis4369/src/master/a1/README.md)

    * Install Pyhton
    * Install R
    * Install R Studio
    * Install Visual Studio Code
    * Create a1_tip_calculator application
    * Provide screenshots of installations
    * create Bitbucket repo
    * complete Bitbucket tutorial (bitbucketstationlocation)
    * Provide git command descriptions

2. [A2 README.md](https://bitbucket.org/mes17/lis4369/src/master/a2/README.md)

    * Create a2_payroll application
    * Provide screenshots of installations

3. [A3 README.md](https://bitbucket.org/mes17/lis4369/src/master/a3/README.md)

    * Create a3_painting_estimator application
    * Provide screenshots of installations

4. [A4 README.md](https://bitbucket.org/mes17/lis4369/src/master/a4/README.md)

    * Create Assignment 4 application
    * Provide screenshots of installations

5. [A5 README.md](https://bitbucket.org/mes17/lis4369/src/master/a5/README.md)

    * Create Assignment 5 application
    * Provide screenshots of installations

6. [P1 README.md](https://bitbucket.org/mes17/lis4369/src/master/p1/README.md)

    * Create Project 1 application
    * Provide screenshots of installations

7. [P2 README.md](https://bitbucket.org/mes17/lis4369/src/master/p2/README.md)

    * Create Project 2 application
    * Provide screenshots of installations
