> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Solutions

## Matthew Stoklosa

### Project 1 Requirements:


1. Requirements:
    
    a. Code and run demo.py. (Note: *be sure* necessary packages are installed!)
    
    b. Then. use it to backward-engineer the screenshots below it.
    
    c. Installing Python packages pip helper videos:

See “Program Requirements” screenshot below.
   
    a.https://www.youtube.com/watch?v=U2ZN104hIcc
    
    b. https://www.youtube.com/watch?v=bij66_Jtoqs

Note: *Be sure* to log in as administrator (command prompt, or sudo LINUX/Mac)


Also, do *your* own research on Python package installing and updating.

2. Be sure to test your program using both IDLE and Visual Studio Code.
 

#### README.md file should include the following items:

* Screenshot of Project 1

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of demo.py running on Idle*:

![demo.py Idle Screenshot](img/p1_idle_1.png)

*Screenshot of demo.py running on Idle*:

![demo.py Idle Screenshot](img/p1_idle_2.png)


*Screenshot of demo.py running in Visual*:

![demo.py Visual Screenshot](img/p1_visual_1.png)

*Screenshot of demo.py running in Visual*:

![demo.py Visual Screenshot](img/p1_visual_2.png)
*Screenshot of demo.py running in Visual*:

![demo.py Visual Screenshot](img/p1_visual_3.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
