> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Solutions

## Matthew Stoklosa

### Project 2 Requirements:


1. Requirements:

    a. Use Assignment 5 screenshotsand references aboveforthe following requirements:

    b. Backward-engineer the lis4369_p2_requirements.txtfilec.Be sure to include at least two plotsin your README.md file.

2. Be sure to test your program using RStudio.
 

#### README.md file should include the following items:

* Screenshot of Project 2

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Project 2 Screenshots:

*Screenshot of Project 2 running on RStudio*:

![Project 2 running on RStudio Screenshot](img/p2_1.png)

*Screenshot of Project 2 running on RStudio*:

![Project 2 running on RStudio Screenshot](img/p2_2.png)

*Screenshot of Project 2 running on RStudio*:

![Project 2 running on RStudio Screenshot](img/p2_3.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
