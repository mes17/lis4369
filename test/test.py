#!/usr/bin/envpython3

# Program requirments:
print("Square Feet to Acres")
print("\nProgram requirements:\n"
        + "1. Reserch: number of square feet to acre of land.\n"
        + "2. Must use float data type for user input and calculation.\n"
        + "3. Format and round conversion to two decimnal places.")

#Get the total square feet
print("\nInput:")
totalSquareFeet = float(input("Enter the total square feet : "))

#calculate the total acres
totalAcre = totalSquareFeet / 43560

# display results
print("\nOutput:")
print("{:,.2f} square feet".format(totalSquareFeet) +" = {:0.2f}".format(totalAcre) + " acres")